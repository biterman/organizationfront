import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { EmployeeDetailsComponent } from "../employee-details/employee-details.component";

import { EmployeeListComponent } from "../employee-list/employee-list.component";
import { HomeComponent } from "../home/home.component";

const routes = [
    { path: 'home', component: HomeComponent },
    { path: 'employeeList', component: EmployeeListComponent },
    { path: 'employeeDetails/:id', component: EmployeeDetailsComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class RoutingModule {}
