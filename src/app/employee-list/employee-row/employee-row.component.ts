import { Component, Input, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';

@Component({
  selector: 'app-employee-row',
  templateUrl: './employee-row.component.html',
  styleUrls: ['./employee-row.component.less']
})
export class EmployeeRowComponent implements OnInit {

  @Input('employeeData') employeeData: Employee;

  constructor() { }

  ngOnInit(): void {
  }

  openDetails() {
    
  }

}
