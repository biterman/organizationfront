import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Employee } from '../models/employee.model';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.less']
})
export class EmployeeListComponent implements OnInit, OnDestroy {

  loading = true;

  employees: Employee[] = [];
  empoyessSubscription: Subscription;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.empoyessSubscription = this.employeeService.getEmployees().subscribe(data => {
      this.employees = data;
    }, error => {

    }, () => {
      this.loading = false;
    })
  }

  ngOnDestroy(): void {
    this.empoyessSubscription.unsubscribe();
  }

}
