export class Report {
    id: string;
    text: string;
    date: Date;
    empId: string;
    managerId: string;
}