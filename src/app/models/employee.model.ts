export interface Employee {
    id: string;
    firstName: string;
    lastName: string;
    position: string;
    isManager: boolean;
}