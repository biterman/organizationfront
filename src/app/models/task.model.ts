export class Task {
    id: string;
    empId: string;
    text: string;
    assignDate: Date;
    dueDate: Date;
}