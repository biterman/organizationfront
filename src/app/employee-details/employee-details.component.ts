import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { switchMap } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


import { Employee } from '../models/employee.model';
import { Manager } from '../models/manager.model';
import { Task } from '../models/task.model';
import { EmployeeService } from '../services/employee.service';
import { ReportDialogComponent } from './report-dialog/report-dialog.component';
import { Report } from '../models/report.model';
import { TaskDialogComponent } from './task-dialog/task-dialog.component';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.less']
})
export class EmployeeDetailsComponent implements OnInit {

  employee$: Observable<Employee | Manager>;
  manager$: Observable<Employee>;
  subordinate$ : Observable<Employee[] | Manager[]>;
  tasks$: Observable<Task[]>;
  reports$: Observable<Report[]>;
  randomAvatarUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private employeeService: EmployeeService
    ) { }

  ngOnInit(): void {
    this.employee$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.employeeService.getEmployeeDataById(params.get('id'))),
        map(emp => {

          this.randomAvatarUrl = `https://www.gravatar.com/avatar/${emp.id}?d=robohash`;

          if (emp.isManager) {
            this.subordinate$ = this.employeeService.getManagerSubordinates(emp.id);
            this.reports$ = this.employeeService.getReports(emp.id);
          }

          this.manager$ = this.employeeService.getMyManager(emp.id);
          this.tasks$ = this.employeeService.getEmployeeTasks(emp.id);

          return emp;
        })
    );
  }

  report(empId: string, managerId: string): void {
    const dialogRef = this.dialog.open(ReportDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.length > 0) {
        const report: Report = {
          text: result,
          date: new Date(),
          id: 'new',
          empId,
          managerId
        } 
        this.employeeService.reportToManager(report)
        .toPromise()
        .then((res) => {
          console.log('reportToManager res >>> ', res);
        });
      }
    });
  }

  assignTask(empId: string): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe((result: Task) => {
      if (result) {
        console.log(result);
        result.empId = empId;
        this.employeeService.assignTask(result)
        .toPromise()
        .then((res) => {
          console.log('assignTask res >>> ', res);
        });
      }
    });
  }

}

