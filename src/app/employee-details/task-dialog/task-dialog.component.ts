import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.less']
})
export class TaskDialogComponent implements OnInit {

  task: Task;
  minDate: Date;
  maxDate: Date;

  constructor(
    public dialogRef: MatDialogRef<TaskDialogComponent>
  ) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date();
    this.maxDate = new Date(currentYear + 1, 11, 31);
   }

  ngOnInit(): void {
    this.task = new Task();
  }

  submitForm(form: NgForm) {    
    if(form.valid) {
      this.task.assignDate = new Date();
      this.task.id = 'new';
      this.dialogRef.close(this.task);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
