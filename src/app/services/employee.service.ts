import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee.model';
import { Manager } from '../models/manager.model';
import { Report } from '../models/report.model';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {

  private apiUrl = 'https://localhost:5001/api/';

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.apiUrl}Employee`);
  }

  getEmployeeDataById(id: string): Observable<Employee | Manager>{
    return this.http.get<Employee>(`${this.apiUrl}Employee/${id}`);
  }

  getManagerSubordinates(id: string): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.apiUrl}Employee/Subordinate/${id}`)
  }
  getReports(id: string): Observable<Report[]> {
    return this.http.get<Report[]>(`${this.apiUrl}Employee/Reports/${id}`)
  }

  getEmployeeTasks(id: string): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.apiUrl}Employee/Task/${id}`)
  }

  getMyManager(id: string): Observable<Employee> {
    return this.http.get<Employee>(`${this.apiUrl}Employee/MyManager/${id}`);
  }

  assignTask(task: Task) {
    return this.http.post(`${this.apiUrl}Employee/AssignTask`, {...task});
  }

  reportToManager(report: Report) {
    return this.http.post(`${this.apiUrl}Employee/Report`, {...report});
  }

}